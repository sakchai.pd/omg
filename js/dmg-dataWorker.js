
onmessage = function (e){
    let method = (e.data.method || 'GET').toUpperCase();
    let url = e.data.url||'';
    let xhr=typeof XMLHttpRequest!='undefined'?new XMLHttpRequest():new ActiveXObject('Microsoft.XMLHTTP');

    xhr.onreadystatechange=function(){
        if (xhr.readyState == 4 && xhr.status == 200){
            postMessage({readyState: 4, status: 200, responseText: xhr.responseText});
        }
    };
    xhr.open('GET',url,true);
    xhr.send();
}