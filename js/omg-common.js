//** Global Object //
// $g: {
//    screen,
//    display,
//    browser: {type,ver,mobile,touch,svg,canvas},
//    queryString()
//  }
//** **//

$g={
   screen: {"width":screen.width,"height":screen.height},
   display: {"width":window.innerWidth,"height":window.innerHeight}
};
$g.queryString=function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length && query!=='';i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();
$g.browser = (function(){
    var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|CriOS|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!== null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!== null) M.splice(1, 1, tem[1]);
    return {
        "type":(M[0].replace('CriOS','chrome').toLowerCase()),
        "ver":(M[1].toLowerCase()),
        "mobile":(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)),
        "touch":('ontouchstart' in window || window.DocumentTouch && document instanceof window.DocumentTouch || navigator.maxTouchPoints > 0 || window.navigator.msMaxTouchPoints > 0),
        "svg":(document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0") || false),
        "canvas":(!!document.createElement("canvas").getContext || false)};
})();


function loadData(o){
    if(o){
        let method = (o.method || 'GET').toUpperCase();
        let url = o.url||'';
        let cb = (o.success || function(){});
        let xhr=typeof XMLHttpRequest!='undefined'?new XMLHttpRequest():new ActiveXObject('Microsoft.XMLHTTP');

        xhr.onloadstart=function(e){
        };
        xhr.onreadystatechange=function(){
            if (xhr.readyState == 4 && xhr.status == 200){
                cb(xhr.responseText,o);
            }
        };
        xhr.onprogress=function(e){
        };
        xhr.ontimeout=function (e){
        }
        xhr.onloadend=function(e){
        };
        xhr.open('GET',url,true);
        xhr.send();
    } else { throw new Error('Error: loadData(param) does not exist.') }
}
